<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../ressources/css/style.css">

    <meta charset="UTF-8">
    <title><?php /** @var string $titre */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
        </ul>
    </nav>
    <h1>Liste des utilisateurs</h1>
</header>
<main>
    <?php
/** @var string $cheminCorpsVue */
    require __DIR__ . "/{$cheminCorpsVue}";

    ?>
</main>
<footer>
    <p> Site de mindset millionaire</p>
</footer>
</body>
</html>