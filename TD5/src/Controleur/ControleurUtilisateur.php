<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Utilisateur;

//require_once __DIR__ . ('/../Modele/Utilisateur.php'); // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        /** @var string $titre */
        /** @var string $cheminCorpsVue */

        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/liste.php";

        $utilisateurs = Utilisateur::recupererUtilisateurs();
        //appel au modèle pour gérer la BD
        if (count($utilisateurs) == 0) {
            ControleurUtilisateur::afficherVue("utilisateur/erreur.php");
        } else {
//            require('../vue/utilisateur/liste.php');  //"redirige" vers la vue
//            ControleurUtilisateur::afficherVue('../vue/utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "utilisateurs" => $utilisateurs]);
        }
    }

    public static function afficherDetail(): void
    {

        /** @var string $titre */
        /** @var string $cheminCorpsVue */

        $titre = "Infos utilisateur";
        $cheminCorpsVue = "utilisateur/detail.php";

        $l = $_GET['login'];
        $use = Utilisateur::recupererUtilisateurParLogin($l);
        if (!isset($use)) {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
        } else {
            ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "use" => $use]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire(): void
    {
        /** @var string $titre */
        /** @var string $cheminCorpsVue */

        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/utilisateurCree.php";

        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $u = new Utilisateur($login, $nom, $prenom);

        $u->ajouter();
        $utilisateurs = Utilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
//        ControleurUtilisateur::afficherListe();
    }

}

?>