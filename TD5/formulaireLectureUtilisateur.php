<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <form method="get" action="formulaireLectureUtilisateur.php">
                <fieldset>
                    <legend>Retrouve un utilisateur par son login :</legend>
                    <p>
                        <label for="login_id">Login</label> :
                        <input type="text" placeholder="Ex : leblancj" name="login" id="login_id" required/>
                    </p>
                    <p>
                        <input type="submit" value="Envoyer" />
                    </p>
                </fieldset>
            </form>
        </div>
        <?php

        require_once  __DIR__.'/src/Modele/ConnexionBaseDeDonnees.php';
        require_once __DIR__.'/src/Modele/ModeleUtilisateur.php';

        function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
            $sql = "SELECT * FROM utilisateur2 WHERE login=:loginTag";
            $values=array(
                    "loginTag"=>$login
            );
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $pdoStatement->execute($values);
            echo "<p>J'effectue la requête $pdoStatement <pre></pre></p>";
            $utilisateurTableau = $pdoStatement->fetch();
            if ($utilisateurTableau !== false) {
                return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurTableau);
            }
            return null;
        }

        if (isset($_GET['login'])) {
            $u = recupererUtilisateurParLogin($_GET['login']);
            echo $u;
        }
        ?>
    </body>
</html>
