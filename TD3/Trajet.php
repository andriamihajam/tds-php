<?php



require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet
{

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int   $id,
        ?string $depart,
        ?string $arrivee,
        ?string $date,
        ?int    $prix,
        ?string $loginConducteur,
        ?bool   $nonFumeur,
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = new DateTime($date);
        $this->prix = $prix;
        $this->conducteur = Utilisateur::recupererUtilisateurParLogin($loginConducteur);
        $this->nonFumeur = $nonFumeur;
        if (count($this->recupererPassagers())==0){
            $this->passagers=[];
        }
        else {
            $this->setPassagers($this->recupererPassagers());
        }

    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }


    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {

        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $trajetTableau["date"], // À changer, nananana
            $trajetTableau["prix"],
            $trajetTableau["conducteurLogin"],
            $trajetTableau["nonFumeur"],// À changer ? nanannaan
         );

    }
    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }


    public function ajouter(): void
    {
        $sql = "INSERT INTO trajet (id,depart,arrivee,date,prix,conducteurLogin,nonFumeur) VALUES (:idTag,:departTag,:arriveeTag,:dateTag,:prixTag,:conducteurTag,:nomFumeurTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTag" => $this->id,
            "departTag" => $this->depart,
            "arriveeTag" => $this->arrivee,
            "dateTag" => $this->date->format("Y-m-d"),
            "prixTag" => $this->prix,
            "conducteurTag" => $this->conducteur->getLogin(),
            "nomFumeurTag" => $this->nonFumeur
        );
        $pdoStatement->execute($values);

    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    private function recupererPassagers(): array
    {
        $sql = "SELECT login
              FROM utilisateur u
              JOIN passager p ON u.login=p.passagerLogin
               WHERE trajetId=:idTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTag" => $this->id,
        );
        $pdoStatement->execute($values);
        $passagers = [];
        foreach ($pdoStatement as $passagerDuTrajet) {
            $passagers[]= ModeleUtilisateur::recupererUtilisateurParLogin($passagerDuTrajet['login']);
        }
           return $passagers;
    }
    public function supprimerPassager(string $passagerLogin): bool {
        return true;
    }

}
