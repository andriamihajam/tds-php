<?php


require_once "ConnexionBaseDeDonnees.php";
require_once "ConfigurationBaseDeDonnees.php";

class Utilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;


    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager=null;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager==null){
            $this->setTrajetsCommePassager($this->recupererTrajetsCommePassager());
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    private function recupererTrajetsCommePassager(): array
    {
        $sql = 'SELECT trajetId 
        FROM passager p 
        WHERE passagerLogin=:idTag';
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTag" => $this->login
        );
        $pdoStatement->execute($values);
        $trajetPassager = [];
        foreach ($pdoStatement as $p) {
            $trajetPassager[] = $p['trajetId'];
        }
         return $trajetPassager;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
        return $utilisateur;
    }

    public static function recupererUtilisateurs(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');
        $tabUsers = [];

        foreach ($pdoStatement as $user) {
            $tabUsers[] = ModeleUtilisateur::construireDepuisTableauSQL($user);
        }

        return $tabUsers;

    }

    public static function recupererUtilisateurParLogin(string $login): ?ModeleUtilisateur
    {
        $sql = "SELECT * FROM utilisateur  WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant

        $test = $pdoStatement->fetch();

        if (!$test) {
            return null;
        }
        return ModeleUtilisateur::construireDepuisTableauSQL($test);
    }

    public function ajouter(): void
    {
        $sql = "INSERT INTO Utilisateur (login,nom,prenom) VALUES (:loginTag,:nomTag,:prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom
        );
        $pdoStatement->execute($values);
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $carac = substr($login, 0, 64);
        $this->login = $carac;
    }

    public function __toString(): string
    {
        return "$this->nom $this->prenom  $this->login ";
    }
}