<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;
    private string $mdpHache;
    private bool $estAdmin;
    private string $email;
    private string $emailAValider;
    private string $nonce;

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->setTrajetsCommePassager(UtilisateurRepository::recupererTrajetsCommePassager($this));
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }


    public function getLogin(): string
    {
        return $this->login;
    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }


    // un setter
    public function setNom(string $nom)
    {

        $this->nom = $nom;
    }

    public function setLogin(string $login)
    {

        $login = substr($login, 0, 64);
        $this->login = $login;
    }

    public function setPrenom(string $prenom)
    {

        $this->prenom = $prenom;
    }


    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        string $mdpHache,
        bool $estAdmin,
        string $email,
        string $emailAValider,
        string $nonce
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->mdpHache = $mdpHache;
        $this->estAdmin=$estAdmin;
        $this->email=$email;
        $this->emailAValider=$emailAValider;
        $this->nonce=$nonce;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }




//    public function __toString(): string
//    {
//        return "nom :  {$this->getNom()} , prenom : {$this->getPrenom()}, login : {$this->getLogin()}";
//    }
}

