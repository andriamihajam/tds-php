<!doctype html>
<html lang="fr">
<body>

<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */
$loginUrl = rawurlencode($utilisateur->getLogin());?>

<h1>Détail utilisateur :</h1>
<lu>
    <li>login : <?php echo htmlspecialchars($utilisateur->getLogin()) ?></li>
    <li>nom : <?php echo htmlspecialchars($utilisateur->getNom()) ?></li>
    <li>prenom : <?php echo htmlspecialchars($utilisateur->getPrenom()) ?></li>
    <li> admin: <?php echo htmlspecialchars($utilisateur->isEstAdmin()) ?> </li>
    <li> mdp : <?php echo htmlspecialchars($utilisateur->getMdpHache())?> </li>

    <?php if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
        echo '<li> <a href="controleurFrontal.php?action=supprimer&login=' . $loginUrl . '&controleur=utilisateur" >Supprimer</a></li>';
        echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginUrl . '">  Update </a>';

    } ?>
</lu>

</body>
</html>



