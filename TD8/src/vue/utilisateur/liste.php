<!DOCTYPE html>
<html>

<body>
<?php
/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $login = htmlspecialchars($utilisateur->getLogin());
    $loginurl = rawurlencode($utilisateur->getLogin());

    echo '<p> Utilisateur de login <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginurl .'">' . $utilisateur->getNom()." ". $utilisateur->getPrenom() . '</a>';
    if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()){
        echo ' <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginurl . '">  Update </a>';
    }
}

?>

<br>
<h4><a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un nouvelle utilisateur</a></h4>
</body>
</html>