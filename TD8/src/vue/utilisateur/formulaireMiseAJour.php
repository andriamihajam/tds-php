<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */
?>
<!DOCTYPE html>
<html>

<body>

<form method="get" action="/tds-php/TD8/web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" name="mdp" id="mdp_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" name="mdp2" id="mdp2_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp3_id">Confirmation mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" name="mdp3" id="mdp3_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prénom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="<?= $utilisateur->getEmail()?>"  name="email" id="email_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <?php if ($utilisateur->isEstAdmin()) { ?>
                <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" checked>
            <?php } else { ?>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
            <?php } ?>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>


</body>
</html>
