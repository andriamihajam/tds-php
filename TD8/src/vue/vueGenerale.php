
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/tds-php/TD8/ressources/css/navstyle.css">
    <title><?php /** @var string $titre */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"> Controleurs </a>
                <img src="../ressources/img/heart.png" alt="heart" width="50" height="50"  >
            </li>
            <?php
             use App\Covoiturage\Lib\ConnexionUtilisateur;
             echo ConnexionUtilisateur::estConnecte() ? "est connecte" : "n'est pas connecte";
            if (!ConnexionUtilisateur::estConnecte()){
                    echo                 '<li>
                    <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png" alt="add"></a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png" alt="login"  width="50" height="50"></a>
                </li>';
                    }
            else {
                 echo '<li> <a href="controleurFrontal.php?action=afficherDetail&login='. rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte()). '&controleur=utilisateur" ><img src="../ressources/img/userConnected.png" width="50" height="50"></a>';
                echo '<li> <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"  ><img src="../ressources/img/door.png" width="50" height="50"></a>';
            }?>
        </ul>
    </nav>
</header>
<main>
    <?php
    /** @var string $cheminCorpsVue */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de mindset millionaire
    </p>
</footer>
</body>
</html>