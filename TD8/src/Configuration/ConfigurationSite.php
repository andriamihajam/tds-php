<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    public static function getDureeExpirationSession(): int
    {
         return 30 * 60; // 30 minutes
    }
    public static function getURLAbsolue(): string
    {
        return "http://localhost/tds-php/TD8/web/controleurFrontal.php";
    }
    public static function getDebug(): bool{
        return false;
    }
}