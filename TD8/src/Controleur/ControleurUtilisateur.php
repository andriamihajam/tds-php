<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;


class ControleurUtilisateur extends ControleurGenerique
{
     public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            $messageErreur = "le login n'a pas étais préciser";
            ControleurUtilisateur::afficherErreur($messageErreur);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }


    public static function creerDepuisFormulaire(): void
    {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom']) && (isset($_GET['mdp']) && isset($_GET['mdp2']) && $_GET['mdp'] == $_GET['mdp2'])) {

            if (isset($_GET['estAdmin'])) {
                $estAdmin = 1;
            } else {
                $estAdmin = 0;
            }

            if (!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
                ControleurUtilisateur::afficherErreur("Adresse email invalide");
                return;
            }

            $utilisateur = ControleurUtilisateur::construireDepuisFormulaire(
                array("login" => $_GET['login'],
                    "nom" => $_GET['nom'],
                    "prenom" => $_GET['prenom'],
                    "mdp" => $_GET['mdp'],
                    "estAdmin" => $estAdmin,
                    "email" => $_GET['email'],
                    "emailAValider" => $_GET['email'],
                    "nonce" => MotDePasse::genererChaineAleatoire())
            );

            $o = (new UtilisateurRepository())->ajouter($utilisateur);
            if (!$o) {
                echo "erreur";
            }
            else {
                VerificationEmail::envoiEmailValidation($utilisateur);
            }

            $utilisateurs = (new UtilisateurRepository())->recuperer();
            (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "utilisateurCree", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        } else {
            ControleurUtilisateur::afficherErreur("Login ou prénom manquant ou mot de passe non identique");
        }
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        if (isset($_GET['login'])) {
            if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
                ControleurUtilisateur::afficherErreur("Vous n'avez pas le droit de supprimer un autre utilisateur");
                return;
            }
            $login = $_GET['login'];
            $rep = (new UtilisateurRepository())->supprimer($login);
            if ($rep) {
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurSupprime", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", 'utilisateurs' => $utilisateurs, "login" => $login]);
            } else {
                ControleurUtilisateur::afficherErreur("Une erreur est survenue lors de la suppression");
            }
        }
    }


    public static function afficherFormulaireMiseAJour(): void
    {
        if (isset($_GET['login'])) {
            if (ConnexionUtilisateur::estConnecte() || (ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::estAdministrateur() && (new UtilisateurRepository())->recupererParClePrimaire(ConnexionUtilisateur::getLoginUtilisateurConnecte()) != null)) {
                $login = $_GET['login'];
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
                if (is_null($utilisateur)) {
                    ControleurUtilisateur::afficherErreur("le login correspond a aucun utilisateur");
                } else {
                    (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurFormulaireMAJ", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", 'utilisateur' => $utilisateur, "login" => $login]);
                }
            }
        } else {
            ControleurUtilisateur::afficherErreur("le login n'est pas préciser");
        }
    }

    public static function mettreAJour(): void
    {
        var_dump($_GET);

        if (!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom']) || !isset($_GET['mdp']) || !isset($_GET['mdp2']) || !isset($_GET['mdp3']) || !isset($_GET['email'])) {
            ControleurUtilisateur::afficherErreur("Données manquantes");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            ControleurUtilisateur::afficherErreur("Vous n'avez pas le droit de modifier un autre utilisateur");
            return;
        }

        $u = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($u == null) {
            ControleurUtilisateur::afficherErreur("Utilisateur introuvable");
            return;
        }

        if ($_GET['mdp2'] != $_GET['mdp3']) {
            ControleurUtilisateur::afficherErreur("Les deux nouveaux mots de passe ne coïncident pas");
            return;
        }

        if (!ConnexionUtilisateur::estAdministrateur()){
            if (!MotDePasse::verifier($_GET['mdp'], $u->getMdpHache())) {
                ControleurUtilisateur::afficherErreur("Le mot de passe saisi est erroné");
                return;
            }
        }
        if (!MotDePasse::verifier($_GET['mdp'], $u->getMdpHache()) && $u->getMdpHache() != null) {
            printf("mdp : %s, mdpHache : %s", $_GET['mdp'], $u->getMdpHache());
            ControleurUtilisateur::afficherErreur("Le mot de passe saisi est erroné");
            return;
        }

        if ($_GET['login'] != ConnexionUtilisateur::getLoginUtilisateurConnecte()) {
            ControleurUtilisateur::afficherErreur("L'utilisateur connecté est différent de l'utilisateur saisi");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur == null) {
            ControleurUtilisateur::afficherErreur("Utilisateur introuvable");
            return;
        }
        $utilisateur->setLogin($_GET['login']);
        $utilisateur->setNom($_GET['nom']);
        $utilisateur->setPrenom($_GET['prenom']);
        $mdp = MotDePasse::hacher($_GET["mdp"]);
        $utilisateur->setMdpHache($mdp);
        $admin = 0;
        if (isset($_REQUEST["estAdmin"]) && ConnexionUtilisateur::estAdministrateur()) {
            $admin = 1;
        }        $utilisateur->setEstAdmin($admin);

        if ($_GET['email'] != $utilisateur->getEmail()) {
            if (!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
                ControleurUtilisateur::afficherErreur("Adresse email invalide");
                return;
            }
            $utilisateur->setEmailAValider($_GET['email']);
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
        }
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurMiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $utilisateur->getLogin(), 'utilisateurs' => $utilisateurs]);
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur(
            $tableauDonneesFormulaire["login"],
            $tableauDonneesFormulaire["nom"],
            $tableauDonneesFormulaire["prenom"],
            MotDePasse::hacher($tableauDonneesFormulaire["mdp"]),
            $tableauDonneesFormulaire["estAdmin"],
            "",
            $tableauDonneesFormulaire["email"],
            MotDePasse::genererChaineAleatoire()
        );
    }

    public static function afficherFormulaireConnexion(): void
    {
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Formulaire connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void
    {
        if (isset($_GET['login']) && isset($_GET['mdp'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (($utilisateur != null && MotDePasse::verifier($_GET['mdp'], $utilisateur->getMdpHache()))) {
                if(!VerificationEmail::aValideEmail($utilisateur) && $utilisateur->getEmailAValider() != ""){
                    ControleurUtilisateur::afficherErreur("Email non validé");
                    return;
                }
                ConnexionUtilisateur::connecter($utilisateur->getLogin());
                ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "utilisateur" => $utilisateur]);
            } else {
                ControleurUtilisateur::afficherErreur("Login et/ou mot de passe manquant.");
            }
        } else {
            ControleurUtilisateur::afficherErreur("Login et/ou mot de passe manquant.");
        }
    }

    public static function deconnecter(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ConnexionUtilisateur::deconnecter();
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Deconnexion", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", "utilisateurs" => $utilisateurs]);
    }
public static function validerEmail():void
{
    if (isset($_GET['login']) && isset($_GET['nonce']) && VerificationEmail::traiterEmailValidation($_GET['login'], $_GET['nonce'])) {
        $utilisateur=(new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur==null){
            ControleurUtilisateur::afficherErreur("Utilisateur introuvable");
            return;
        }
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre" => "Email validé", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
    } else {
        ControleurUtilisateur::afficherErreur("Erreur lors de la validation de l'email");

    }
}

}

?>