<?php

class Utilisateur
{
    private string $login;
    private string  $nom;
    private string $prenom;

    public   function getNom() : string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function __construct( string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getPrenom():string
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin():string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $carac = substr($login, 0, 64);
        $this->login = $carac;
    }

    public function __toString():string{
         return "$this->nom $this->prenom  $this->login ";
    }
}