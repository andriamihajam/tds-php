<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php

//        $prenom = "Marc";
//        echo "Bonjour\n" . $prenom;
//        echo "Bonjour\n $prenom";
//        echo 'Bonjour\n $prenom';
//
//        echo $prenom;
//        echo "$prenom";


        $nom = "Uzuhiko";
        $prenom = "Rasen";
        $login = "gan";

        echo "<p> Utilisateur $nom $prenom de login $login </p>";

        $utilisateur = [
                'nom'=> 'Hello',
                 'prenom'=> 'World',
                 'login' =>'Okay'
        ];
        var_dump($utilisateur);

       echo "<p> Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]</p>";

        $utilisateurs = [
           'utilisateur1','utilisateur2','utilisateur3'
        ];

//        var_dump($utilisateurs);

        echo  "<p> Liste des utilisateurs</p> <br>";
        if (count($utilisateurs)==0){
            echo "<p> Liste vide</p>";
        }
        else {
       echo "<ul>";
        foreach ($utilisateurs as $user ){
            echo "<li>$user</li>";
        }
       echo "</ul>";
}

        // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
         // $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          //echo $texte;
        ?>
    </body>
</html> 