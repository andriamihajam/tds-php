<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

//require_once __DIR__.'/../src/Controleur/ControleurUtilisateur.php';
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';


// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


$controleur = "utilisateur";
if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}

$temp = ucfirst($controleur);
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur$temp";
  if (class_exists($nomDeClasseControleur)) {
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        $methodes = get_class_methods($nomDeClasseControleur);
        if (!in_array($action, $methodes)) {
            $nomDeClasseControleur::afficherErreur("méthode inexistante");
        } else {
            $nomDeClasseControleur::$action();
        }
    }
} else {
    ControleurUtilisateur::afficherErreur("Controleur inexistant");
}

?>