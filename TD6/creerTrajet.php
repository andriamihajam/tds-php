 <?php
    require_once __DIR__ . "/src/Modele/Utilisateur.php";
    require_once __DIR__ . "/src/Modele/ConnexionBaseDeDonnees.php";
    require_once __DIR__ . "/TrajetSansRepertoire.php";
    if (!isset($_POST['depart']) || !isset($_POST['arrivee']) || !isset($_POST['date']) || !isset($_POST['prix']) || !isset($_POST['conducteurLogin']) || !isset($_POST['nonFumeur'])) {
        echo "Vous n'avez pas rempli les champs";
    }

    else {

        $depart= $_POST['depart'];
        $arrivee=$_POST['arrivee'];
        $date=$_POST['date'];
        $prix=$_POST['prix'];
        $login=$_POST['conducteurLogin'];
        $nomFumeur=$_POST['nonFumeur'];
        $t=new Trajet(null,$depart,$arrivee,$date,$prix,$login,$nomFumeur);
        $t->ajouter();
     }
    ?>