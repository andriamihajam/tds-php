<p> <?php
    require_once __DIR__ . "/src/Modele/Utilisateur.php";
    require_once __DIR__ . "/src/Modele/ConnexionBaseDeDonnees.php";
    if (!isset($_POST['nom']) || !isset($_POST['prenom']) || !isset($_POST['login'])) {
        echo "Vous n'avez pas rempli les champs";
    } else {
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $login = $_POST['login'];
        $u = new ModeleUtilisateur($nom, $prenom, $login);
        $sql = "INSERT INTO Utilisateur (login,nom,prenom) VALUES (:loginTag,:nomTag,:prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $login,
            "nomTag" => $nom,
            "prenomTag" => $prenom
        );
        $pdoStatement->execute($values);
//        echo $u;
    }
    ?> </p>

