<?php
namespace App\Covoiturage\Modele\HTTP;
class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        $valeurSerial = serialize($valeur);
        if ($dureeExpiration === null) {
            setcookie($cle, $valeurSerial);
        }

        else {
            setcookie($cle, $valeurSerial, time() + $dureeExpiration);
        }
    }


    public static function lire(string $cle): mixed
    {
        return unserialize($_COOKIE[$cle]);
    }
    public static function supprimer($cle) : void{
        unset($_COOKIE[$cle]);
        setcookie ("TestCookie", "", 1);
    }

}