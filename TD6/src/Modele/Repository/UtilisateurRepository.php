<?php
namespace App\Covoiturage\Modele\Repository;

//use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{

    private static function recupererTrajetsCommePassager(Utilisateur $u): array
    {
        $sql = 'SELECT trajetId 
        FROM passager p 
        WHERE passagerLogin=:idTag';
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTag" => $u->getLogin()
        );
        $pdoStatement->execute($values);
        $trajetPassager = [];
        foreach ($pdoStatement as $p) {
            $trajetPassager[] = $p['trajetId'];
        }
        return $trajetPassager;
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        $utilisateur = new Utilisateur($objetFormatTableau['login'], $objetFormatTableau['nom'], $objetFormatTableau['prenom']);
        return $utilisateur;
    }

    protected function getNomTable(): string
    {
        return "Utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }
}