<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository
{
    public function ajouter(AbstractDataObject $objet): bool
    {
        $sql = "INSERT INTO " . $this->getNomTable() . " (";
        $colonnes = $this->getNomsColonnes();
        for ($i = 0; $i < count($colonnes); $i++) {
            $sql .= $colonnes[$i];
            if ($i < count($colonnes) - 1) {
                $sql .= ", ";
            }
        }
        $sql .= ") VALUES (";
        for ($i = 0; $i < count($colonnes); $i++) {
            $sql .= ":" . $colonnes[$i] . "Tag";
            if ($i < count($colonnes) - 1) {
                $sql .= ", ";
            }
        }
        $sql .= ")";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($this->formatTableauSQL($objet));
        return true;
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $colonnes = $this->getNomsColonnes();
        $sql = "UPDATE " . $this->getNomTable() . " SET ";
        for ($i = 0; $i < count($colonnes); $i++) {
            $sql .= $colonnes[$i] . " = :" . $colonnes[$i] . "Tag";
            if ($i < count($colonnes) - 1) {
                $sql .= ", ";
            }
        }
//        UPDATE Utilisateur SET login = :loginTag, nom = :nomTag, prenom = :prenomTagWHERE login = :loginTag

        $sql .= " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire() . "Tag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($this->formatTableauSQL($objet));
    }

    public function supprimer(string $login): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $login
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $login): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant

        $test = $pdoStatement->fetch();

        if (!$test) {
            return null;
        }
        return ($this->construireDepuisTableauSQL($test));

    }

    protected abstract function getNomTable(): string;

    public function recuperer(): array
    {

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM ' . $this->getNomTable());
        $tabUsers = [];

        foreach ($pdoStatement as $user) {
            $tabUsers[] = ($this->construireDepuisTableauSQL($user));
        }
        return $tabUsers;
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

}