<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;

class TrajetRepository extends AbstractRepository
{
    public static function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT login
              FROM utilisateur u
              JOIN passager p ON u.login=p.passagerLogin
               WHERE trajetId=:idTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTag" => $trajet->getId(),
        );
        $pdoStatement->execute($values);
        $passagers = [];

        foreach ($pdoStatement as $passagerDuTrajet) {
            $passagers[] = (new UtilisateurRepository())->recupererParClePrimaire($passagerDuTrajet['login']);
        }
        return $passagers;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Trajet
    {
        return new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            $objetFormatTableau["date"], // À changer, nananana
            $objetFormatTableau["prix"],
            $objetFormatTableau["conducteurLogin"],
            $objetFormatTableau["nonFumeur"],// À changer ? nanannaan
        );
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Trajet $utilisateur */

        return array(
            "idTag" => $utilisateur->getId(),
            "departTag" => $utilisateur->getDepart(),
            "arriveeTag" => $utilisateur->getArrivee(),
            "dateTag" => $utilisateur->getDate()->format('Y-m-d'),
            "prixTag" => $utilisateur->getPrix(),
            "conducteurLoginTag" => $utilisateur->getConducteur()->getLogin(),
            "nonFumeurTag" => $utilisateur->isNonFumeur(),
        );
    }

}