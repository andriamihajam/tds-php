<?php
namespace App\Covoiturage\Modele\DataObject;
use \DateTime;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;

class Trajet extends AbstractDataObject
{

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

//    /**
//     * @throws \DateMalformedStringException
//     */
    public function __construct(
        ?int    $id,
        ?string $depart,
        ?string $arrivee,
        ?string $date,
        ?int    $prix,
        ?string $loginConducteur,
        ?bool   $nonFumeur,
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = new DateTime($date);
        $this->prix = $prix;
        $this->conducteur = (new UtilisateurRepository())->recupererParClePrimaire($loginConducteur);
        $this->nonFumeur = $nonFumeur;
        if (count(TrajetRepository::recupererPassagers($this)) == 0) {
            $this->passagers = [];
        } else {
            $this->setPassagers(TrajetRepository::recupererPassagers($this));
        }
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }


    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }
}

