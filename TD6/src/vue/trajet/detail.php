<?php
/** @var Trajet $use */
$IdHTML = htmlspecialchars($use->getId());
$DepartHTML = htmlspecialchars($use->getDepart());
$ArriveeHTML = htmlspecialchars($use->getArrivee());

echo "ID :" . $IdHTML . '<br>';
echo "Départ :" . $DepartHTML . '<br>';
echo "Arrivée :" . $ArriveeHTML . '<br>';