<?php

/** @var Trajet[] $trajets */


foreach ($trajets as $trajet) {
    $id = $trajet->getId();
    $IdHTML = htmlspecialchars($trajet->getId());
    $IdURL = rawurlencode($trajet->getId());
    $DepartHTML = htmlspecialchars($trajet->getDepart());
    $ArriveeHTML = htmlspecialchars($trajet->getArrivee());
    $DateHTML = htmlspecialchars($trajet->getDate()->format('Y-m-d H:i:s'));
    $PrixHTML = htmlspecialchars($trajet->getPrix());
    $ConducteurLoginHTML = htmlspecialchars($trajet->getConducteur()->getLogin());

    echo "Le trajet numéro $IdHTML part de $DepartHTML pour arriver à $ArriveeHTML le $DateHTML, il coûte $PrixHTML € et est conduit par $ConducteurLoginHTML." . "<br>";
    echo "<a href='controleurFrontal.php?action=afficherDetail&login=$IdHTML&controleur=trajet'>Infos en +</a> &nbsp; 
    <a href='controleurFrontal.php?action=supprimer&login=$IdURL&controleur=trajet'>Supprimer</a>&nbsp; 
    <a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&id=$id&controleur=trajet'> Update</a><br>";
    echo "<br>";
}
    echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet'>Envie de créer un nouveau trajet ?</a>";

?>
