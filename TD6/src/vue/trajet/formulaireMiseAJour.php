<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<div>
    <form method="get" action="tds-phpTD6/web/controleurFrontal.php">
        <fieldset>
            <input type='hidden' name='controleur' value='trajet'>
            <?php /** @var Trajet $trajet **/?>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='id' value='<?= $trajet->getId() ?>'>
            <legend>Mon formulaire :</legend>

            <p>
                <label for="depart_id">Depart</label> :
                <input type="text" value="<?= $trajet->getDepart()?>" name="depart" id="depart_id" required/>
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" value="<?= $trajet->getArrivee()?>" name="arrivee" id="arrivee_id" required/>
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" value="<?= $trajet->getDate()->format('Y-m-d') ?>" name="date" id="date_id"  required/>
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="number" value="<?= $trajet->getPrix() ?>" name="prix" id="prix_id"  required/>
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" value="<?= $trajet->getConducteur()->getLogin() ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="checkbox" <?= $trajet->isNonFumeur() ? 'checked' : '' ?> name="nonFumeur" id="nonFumeur_id"/>
            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
</body>
</html>