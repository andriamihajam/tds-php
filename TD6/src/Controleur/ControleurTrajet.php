<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet;
class ControleurTrajet
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $titre = "Liste des trajets";
        $cheminCorpsVue = "trajet/liste.php";

        $trajets = (new TrajetRepository())->recuperer();
        if (count($trajets) == 0) {
            ControleurTrajet::afficherErreur("aucun trajet à récuperer");
        } else {
            ControleurTrajet::afficherVue("vueGenerale.php", ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "trajets" => $trajets]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        $titre = "Erreur";
        $cheminCorpsVue = "/trajet/erreur.php";
        ControleurTrajet::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "titre"=>$titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }
    public static function afficherDetail(): void
    {
        $titre = "Infos trajets";
        $cheminCorpsVue = "trajet/detail.php";

        $l = $_GET['login'];
        $use = (new TrajetRepository())->recupererParClePrimaire($l);
        if (!isset($use)) {
            ControleurTrajet::afficherErreur("le trajet n'existe pas");
        } else {
            ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "use" => $use]);
        }
    }

    public static function supprimer(): void
    {
        $titre = "Sayonara";
        $login = $_GET['login'];
        $cheminCorpsVue = "/trajet/trajetSupprime.php";
        (new TrajetRepository())->supprimer($_GET['login']);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue("vueGenerale.php", ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "login" => $login, "trajets" => $trajets]);
    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurTrajet::afficherVue('trajet/formulaireCreation.php');
    }

    public static function creerDepuisFormulaireTrajet(): void
    {
        $titre = "Liste des trajets";
        $cheminCorpsVue = "trajet/trajetCree.php";

        $u = ControleurTrajet::construireDepuisFormulaire($_GET);

        (new TrajetRepository())->ajouter($u);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }

    public static function  afficherFormulaireMiseAJour(): void
    {
        $titre = 'Mise à jour Trajet';
        $cheminCorpsVue = 'trajet/formulaireMiseAJour.php';
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }


    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire['depart'];
        $arrivee = $tableauDonneesFormulaire['arrivee'];
        $date = $tableauDonneesFormulaire['date'];
        $prix = $tableauDonneesFormulaire['prix'];
        $conducteurLogin = $tableauDonneesFormulaire['conducteurLogin'];
        $nonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);
        return new Trajet($id, $depart, $arrivee, $date, $prix, $conducteurLogin, $nonFumeur);
    }

    public static function mettreAJour()
    {
        $titre = 'Mis à jour du Trajet';
        $id = $_GET['id'];
        $cheminCorpsVue = 'trajet/trajetMisAJour.php';
        $u=ControleurTrajet::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($u);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue('vueGenerale.php', ["id" => $id, "trajets" => $trajets, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }
}