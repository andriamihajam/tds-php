<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;

//require_once __DIR__ . ('/../Modele/Utilisateur.php'); // chargement du modèle
class ControleurUtilisateur
{


    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/liste.php";

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        //appel au modèle pour gérer la BD
        if (count($utilisateurs) == 0) {
            ControleurUtilisateur::afficherErreur("aucun utilisateur à récuperer");
        } else {
//            require('../vue/utilisateur/liste.php');  //"redirige" vers la vue
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "utilisateurs" => $utilisateurs]);
        }
    }

    public static function afficherDetail(): void
    {
        $titre = "Infos utilisateur";
        $cheminCorpsVue = "utilisateur/detail.php";

        $l = $_GET['login'];
        $use = (new UtilisateurRepository())->recupererParClePrimaire($l);
        if (!isset($use)) {
            ControleurUtilisateur::afficherErreur("le login n'existe pas");
//            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
        } else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "use" => $use]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire(): void
    {
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/utilisateurCree.php";

        $u = ControleurUtilisateur::construireDepuisFormulaire($_GET);

        (new UtilisateurRepository())->ajouter($u);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        $titre = "Erreur";
        $cheminCorpsVue = "/utilisateur/erreur.php";
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }

    public static function supprimer(): void
    {
        $titre = "Sayonara";
        $login = $_GET['login'];
        $cheminCorpsVue = "/utilisateur/utilisateurSupprime.php";
        (new UtilisateurRepository())->supprimer($_GET['login']);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue, "login" => $login, "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour()
    {

        $titre = 'Update';
        $cheminCorpsVue = 'utilisateur/formulaireMiseAJour.php';
        $loginHTML = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($loginHTML);
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["login" => $loginHTML, "utilisateur" => $utilisateur, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }

    public static function mettreAJour()
    {

        $titre = 'Mis à jour Utilisateur';
        $cheminCorpsVue = 'utilisateur/utilisateurMisAJour.php';
        $loginHTML = $_GET['login'];
        $prenomHTML = $_GET['prenom'];
        $nomHTML = $_GET['nom'];
        (new UtilisateurRepository())->mettreAJour(new Utilisateur($loginHTML, $prenomHTML, $nomHTML));
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["login" => $loginHTML, "utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]);
    }


    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        return new Utilisateur($login, $nom, $prenom);
    }

    public static function deposerCookie(): void
    {
//        Cookie::enregistrer("TestCookie", "OkayMK3",  3600);
    }

    public static function lireCookie(): void
    {
        Cookie::supprimer("TestCookie");
//        $cookie = Cookie::lire("TestCookie");
//        echo $cookie;
    }

    public static function contient($cle) : bool{
        return (isset($_COOKIE[$cle]));
    }

}

?>