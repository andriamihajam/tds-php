<?php

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;


// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
} else {
    if (PreferenceControleur::existe()) {
        $controleur = PreferenceControleur::lire();
    } else {
        $controleur = "utilisateur";
    }
 }

$temp = ucfirst($controleur);
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur$temp";
if (class_exists($nomDeClasseControleur)) {
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        $methodes = get_class_methods($nomDeClasseControleur);
        if (!in_array($action, $methodes)) {
            $nomDeClasseControleur::afficherErreur("Méthode inexistante");
        } else {
            $nomDeClasseControleur::$action();
        }
    }
    else {
        $nomDeClasseControleur::afficherErreur("Action inexistante");
    }
} else {
    ControleurUtilisateur::afficherErreur("Controleur inexistant");
}

?>