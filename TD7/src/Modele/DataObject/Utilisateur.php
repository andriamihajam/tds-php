<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    private ?array $trajetsCommePassager;


    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->setTrajetsCommePassager(UtilisateurRepository::recupererTrajetsCommePassager($this));
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }


    public function getLogin(): string
    {
        return $this->login;
    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }


    // un setter
    public function setNom(string $nom)
    {

        $this->nom = $nom;
    }

    public function setLogin(string $login)
    {

        $login = substr($login, 0, 64);
        $this->login = $login;
    }

    public function setPrenom(string $prenom)
    {

        $this->prenom = $prenom;
    }


    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }




//    public function __toString(): string
//    {
//        return "nom :  {$this->getNom()} , prenom : {$this->getPrenom()}, login : {$this->getLogin()}";
//    }
}

