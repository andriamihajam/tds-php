<?php
/** @var Utilisateur $utilisateur */
?>
<!DOCTYPE html>
<html>

<body>

<form method="get" action="/tds-php/TD7/web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prénom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>


</body>
</html>
