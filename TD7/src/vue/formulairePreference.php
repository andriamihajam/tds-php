<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Préférence de Contrôleur</title>
</head>
<body>

<h1>Choisissez votre contrôleur par défaut</h1>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>
    <p>

        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?php use App\Covoiturage\Lib\PreferenceControleur;

        if(PreferenceControleur::existe()){
            if (PreferenceControleur::lire() == "utilisateur") {
                echo "checked";
            }
        }?>>
        <label for="utilisateurId">Utilisateur</label>
    </p>
    <p>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"     <?php

        if(PreferenceControleur::existe()){
            if (PreferenceControleur::lire() == "trajet") {
                echo "checked";
            }
        }?>>
        <label for="trajetId">Trajet</label>
    </p>
    <p>
        <input type="submit" value="Enregistrer">
    </p>
</form>
</body>
</html>