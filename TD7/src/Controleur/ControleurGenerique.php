<?php
namespace  App\Covoiturage\Controleur;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherFormulairePreference() : void
    {
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Choisir son contrôleur", "cheminCorpsVue" => "formulairePreference.php"]);

//        ControleurGenerique::afficherVue("formulairePreference.php");
    }
    public static function enregistrerPreference():void
    {
        $controleur=$_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($controleur);
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Choisir son contrôleur", "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}