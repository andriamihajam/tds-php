<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;


class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            $messageErreur = "le login n'a pas étais préciser";
            ControleurUtilisateur::afficherErreur($messageErreur);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }


    public static function creerDepuisFormulaire(): void
    {

        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur = ControleurUtilisateur::construireDepuisFormulaire(array("login" => $_GET['login'], "nom" => $_GET['nom'], "prenom" => $_GET['prenom']));
            (new UtilisateurRepository())->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "utilisateurCree", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        } else {
            $messageErreur = "il manque login OR nom OR prenom";
            ControleurUtilisateur::afficherErreur($messageErreur);
        }
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $rep = (new UtilisateurRepository())->supprimer($login);
            if ($rep) {
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurSupprime", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", 'utilisateurs' => $utilisateurs, "login" => $login]);
            } else {
                ControleurUtilisateur::afficherErreur("un problème est survenu lors de la suppression");
            }

        } else {
            ControleurUtilisateur::afficherErreur("le login n'est pas préciser");
        }

    }


    public static function afficherFormulaireMiseAJour()
    {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

            if (is_null($utilisateur)) {
                ControleurUtilisateur::afficherErreur("le login correspond a aucun utilisateur");
            } else {
                (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurFormulaireMAJ", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", 'utilisateur' => $utilisateur, "login" => $login]);
            }

        } else {
            ControleurUtilisateur::afficherErreur("le login n'est pas préciser");
        }
    }

    public static function mettreAJour()
    {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur = ControleurUtilisateur::construireDepuisFormulaire(array("login" => $_GET['login'], "nom" => $_GET['nom'], "prenom" => $_GET['prenom']));
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();

            (new ControleurUtilisateur)->afficherVue('vueGenerale.php', ["titre" => "utilisateurMiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $utilisateur->getLogin(), 'utilisateurs' => $utilisateurs]);
        } else {
            ControleurUtilisateur::afficherErreur("le login/nom/prenom n'a pas étais préciser");
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"]);
    }

    public static function deposerCookie(): void
    {
//        Cookie::enregistrer("TestCookie", "OkayMK3",  3600);
//        Session::getInstance()->detruire();

    }

    public static function lireCookie(): void
    {
//        Cookie::supprimer("TestCookie");
//        $cookie = Cookie::lire("TestCookie");
//        echo $cookie;
//        Session::getInstance()->supprimer("utilisateur");
//        echo Session::getInstance()->lire("utilisateur");
    }

    public static function contient($cle): bool
    {
        return (isset($_COOKIE[$cle]));
    }

    public static function demarrerSession(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Deku");
        echo $session->lire("utilisateur");
        $session->verifierDerniereActivite();
//        echo $session->contient("utilisateur");
    }

}

?>