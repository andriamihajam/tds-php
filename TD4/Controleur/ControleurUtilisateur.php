<?php


require_once('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        //appel au modèle pour gérer la BD
        if (count($utilisateurs) == 0) {
            ControleurUtilisateur::afficherVue("../vue/utilisateur/erreur.php");
        } else {
//            require('../vue/utilisateur/liste.php');  //"redirige" vers la vue
            ControleurUtilisateur::afficherVue('../vue/utilisateur/liste.php',["utilisateurs"=>$utilisateurs]);
        }
    }

    public static function afficherDetail(): void
    {
        $l = $_GET['login'];
        $use = ModeleUtilisateur::recupererUtilisateurParLogin($l);
        if (!isset($use)) {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
        } else {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/detail.php', ["use" => $use]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $u = new ModeleUtilisateur($login, $nom, $prenom);

        $u->ajouter();
        ControleurUtilisateur::afficherListe();
    }

}

?>