<?php


class Utilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }


    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
        return $utilisateur;
    }
    public static function recupererUtilisateurs() : array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');
        $tabUsers=[];

        foreach ($pdoStatement as $user ) {
            $tabUsers[]=ModeleUtilisateur::construireDepuisTableauSQL($user);
         }

        return $tabUsers;

    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $carac = substr($login, 0, 64);
        $this->login = $carac;
    }

    public function __toString(): string
    {
        return "$this->nom $this->prenom  $this->login ";
    }
}